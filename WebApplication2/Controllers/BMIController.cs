﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(float weight,float height)
        {
            float m = height / 100;
            float BMI = weight / (m * m);
            string level = "";
            if (BMI < 18.5)
            {
                level = "體重過輕";
            }
            else if (18.5 <= BMI && BMI < 24)
            {
                level = "正常範圍";
            }
            else if (24 <= BMI && BMI < 27)
            {
                level = "過重";
            }
            else if (27 <= BMI && BMI < 30)
            {
                level = "輕度肥胖";
            }
            else if (30 <= BMI && BMI < 35)
            {
                level = "中度肥胖";
            }
            else if (35 <= BMI)
            {
                level = "重度肥胖";
            }


            ViewBag.BMI = BMI;
                ViewBag.level=level;
            return View();
        }
    }
}